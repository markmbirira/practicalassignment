<?php

/**
 * Define database connection values as constants
 */
define('DB_DSN', 'mysql:host=localhost;dbname=test_petstore_db');
define('DB_USER', 'mark');
define('DB_PASSWORD', 'e40db');
define('OPTIONS', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                  PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,));
<?php

/**
 * Redirect to System Dashboard
 */

define('ROOT_PATH', __DIR__);

header('Location: /views/dashboard.php');

<?php

/**
 * Pet Class
 */
class Pet {

    private $conn;

    public function __construct(PDO $db) {
        $this->conn = $db;
    }

    public function createPet($name, $age, $color, $pet_type_id, $petstore_id) {
        try {
            $stmt = $this->conn->prepare("INSERT INTO pet (name, age, color, pet_type_id, petstore_id) 
                                   VALUES (:name, :age, :color, :pet_type_id, :petstore_id)");
            $cursor = $stmt->execute(array('name' => $name , 'age' => $age , 'color' => $color, 
                                 'pet_type_id' => $pet_type_id, 'petstore_id' => $petstore_id)); 
            return $cursor;
        } catch (PDOException $e) {
            echo "Error inserting Pet: " . $e->getErrorMessage();
            return false;
        }
    }

    public function editPet($pet_id, $name, $age, $color, $pet_type_id, $petstore_id) {
        try {
            $stmt = $this->conn->prepare("UPDATE pet SET name = :name, age = :age,
                                   color = :color, pet_type_id = :pet_type_id, petstore_id = :petstore_id
                                   WHERE id = :pet_id");
            $cursor = $stmt->execute(array('name' => $name , 'description' => $description , 'location' => $location, 
                                 'cage_count' => $cage_count, 'petstore_id' => $petstore_id));
            return $cursor;
        } catch (PDOException $e) {
            echo "Error Updating Pet: " . $e->getErrorMessage();
            return false;
        }
    }

    public function readPets($pet_id=NULL) {
        try {
            if ($pet_id) {
                $stmt = $this->conn->prepare('SELECT * from pet WHERE id = :pet_id');
                $cursor = $stmt->execute(array('pet_id' => $pet_id));
            } else {
                $stmt = $this->conn->prepare('SELECT * from pet ORDER BY id DESC LIMIT 20');
                $cursor = $stmt->execute();
            }
            
            return $cursor;
        } catch (PDOException $e) {
            echo "Error reading Pet(s)" . $e->getMessage();
            return false;
        }
    }

    public function deletePet($pet_id) {
        try {
            $stmt = $this->conn->prepare('DELETE from pet WHERE id = :pet_id');
            $cursor = $stmt->execute(array('pet_id' => $pet_id));
            return $cursor;
        } catch (PDOException $e) {
            echo "Error deleting Pet " . $e->getMessage();
            return false;
        }
    }

}
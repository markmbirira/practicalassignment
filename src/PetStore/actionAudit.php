<?php

/**
 * User actions class
 */
class Actions {
    private $conn;

    public function __construct(PDO $db) {
        $this->conn = $db;
    }

    public function createAction($action, $description, $table_name, $record_id, $date, $custodian_id) {
        try {
            $stmt = $this->conn->prepare("INSERT INTO audit_actions (action, description, table_name, record_id, $date, $custodian_id) 
                                   VALUES (:action, :description, :table_name, :record_id, :date, :custodian_id)");
            $cursor = $stmt->execute(array('action' => $action , 'description' => $description , 'table_name' => $table_name, 
                                 'record_id' => $record_id, 'date' => $date, $custodian_id => 'custodian_id')); 
            return $cursor;
        } catch (PDOException $e) {
            echo "Error inserting Action: " . $e->getErrorMessage();
            return false;
        }
    }

    public function editAction($audit_action_id, $action, $description, $table_name, $record_id, $date, $custodian_id) {
        try {
            $stmt = $this->conn->prepare("UPDATE audit_actions SET action = :action, description = :description,
                                   table_name = :table_name, record_id = :record_id, date = :date, custodian_id = :custodian_id
                                   WHERE audit_action_id = :audit_action_id");
            $cursor = $stmt->execute(array('action' => $action , 'description' => $description , 'table_name' => $table_name, 
                                 'record_id' => $record_id, 'date' => $date, 'custodian_id' => $custodian_id));
            return $cursor;
        } catch (PDOException $e) {
            echo "Error Updating Action: " . $e->getErrorMessage();
            return false;
        }
    }

    public function readActions($audit_action_id=NULL) {
        try {
            if ($audit_action_id) {
                $stmt = $this->conn->prepare('SELECT * from audit_actions WHERE audit_action_id = :audit_action_id');
                $cursor = $stmt->execute(array('audit_action_id' => $audit_action_id));
            } else {
                $stmt = $this->conn->prepare('SELECT * from audit_actions ORDER BY id DESC LIMIT 20');
                $cursor = $stmt->execute();
            }
            
            return $cursor;
        } catch (PDOException $e) {
            echo "Error reading Actions(s)" . $e->getMessage();
            return false;
        }
    }

    public function deletePet($audit_action_id) {
        try {
            $stmt = $this->conn->prepare('DELETE from audit_actions WHERE audit_action_id = :audit_action_id');
            $cursor = $stmt->execute(array('audit_action_id' => $audit_action_id));
            return $cursor;
        } catch (PDOException $e) {
            echo "Error deleting Action " . $e->getMessage();
            return false;
        }
    }

}
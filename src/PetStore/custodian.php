<?php

/**
 * Custodian Class
 */
class Custodian {

    private $conn;

    public function __construct(PDO $db) {
        $this->conn = $db;
    }

    public function createCustodian($username, $first_name, $last_name, $email, $password) {
        try {
            $stmt = $this->conn->prepare("INSERT INTO custodian (username, firstname, lastname, email, password) 
                                   VALUES (:username, :fisrt_name, :last_name, :email, :password)");
            $cursor = $stmt->execute(array('username' => $username , 'firstname' => $first_name , 'last_name' => $last_name, 
                                 'email' => $email, 'password' => $password)); 
            return $cursor;
        } catch (PDOException $e) {
            echo "Error inserting Custodian: " . $e->getErrorMessage();
            return false;
        }
    }

    public function editCustodian($custodian_id, $username, $first_name, $last_name, $email, $password) {
        try {
            $stmt = $this->conn->prepare("UPDATE custodian SET username = :username, first_name = :first_name,
                                   last_name = :last_name, email = :email, password = :password
                                   WHERE id = :custodian_id");
            $cursor = $stmt->execute(array('username' => $username , 'first_name' => $first_name , 'last_name' => $last_name, 
                                 'email' => $email, 'password' => $password, 'custodian_id' => $custodian_id));
            return $cursor;
        } catch (PDOException $e) {
            echo "Error Updating Custodian: " . $e->getErrorMessage();
            return false;
        }
    }

    public function readCustodian($custodian_id=NULL) {
        try {
            if ($custodian_id) {
                $stmt = $this->conn->prepare('SELECT * from custodian WHERE id = :custodian_id');
                $cursor = $stmt->execute(array('custodian_id' => $custodian_id));
            } else {
                $stmt = $this->conn->prepare('SELECT * from custodian ORDER BY id DESC LIMIT 20');
                $cursor = $stmt->execute();
            }
            
            return $cursor;
        } catch (PDOException $e) {
            echo "Error reading Custodian(s)" . $e->getMessage();
            return false;
        }
    }

    public function deleteCustodian($custodian_id) {
        try {
            $stmt = $this->conn->prepare('DELETE from custodian WHERE id = :custodian_id');
            $cursor = $stmt->execute(array('custodian_id' => $custodian_id));
            return $cursor;
        } catch (PDOException $e) {
            echo "Error deleting Custodian " . $e->getMessage();
            return false;
        }
    }

}
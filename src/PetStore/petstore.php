<?php

/**
 * PetStore Class
 */
class PetStore {

    private $conn;

    public function __construct(PDO $db) {
        $this->conn = $db;
    }

    public function createPetStore($name, $description, $location, $cage_count) {
        try {
            $stmt = $this->conn->prepare("INSERT INTO petstore (name, description, location, cage_count) 
                                   VALUES (:name, :description, :location, :cage_count)");
            $cursor = $stmt->execute(array('name' => $name , 'description' => $description , 'location' => $location, 
                                 'cage_count' => $cage_count));
            return $cursor;
        } catch (PDOException $e) {
            echo "Error inserting PetStore: " . $e->getErrorMessage();
            return false;
        }
    }

    public function editPetStore($petstore_id, $name, $description, $location, $cage_count) {
        try {
            $stmt = $this->conn->prepare("UPDATE petstore SET name = :name, description = :description,
                                   location = :location, cage_count = :cage_count WHERE id = :petstore_id");
            $cursor = $stmt->execute(array('name' => $name , 'description' => $description , 'location' => $location, 
                                 'cage_count' => $cage_count, 'petstore_id' => $petstore_id));
            return $cursor;
        } catch (PDOException $e) {
            echo "Error Updating PetStore: " . $e->getErrorMessage();
            return false;
        }
    }

    public function readPetStores($petstore_id=NULL) {
        try {
            if ($petstore_id) {
                $stmt = $this->conn->prepare('SELECT * from petstore WHERE id = :petstore_id');
                $cursor = $stmt->execute(array('petstore_id' => $petstore_id));
            } else {
                $stmt = $this->conn->prepare('SELECT * from petstores ORDER BY id DESC LIMIT 20');
                $cursor = $stmt->execute();
            }
            
            return $cursor;
        } catch (PDOException $e) {
            echo "Error reading PetStore(s)" . $e->getMessage();
            return false;
        }
    }

    public function deletePetstore($petstore_id) {
        try {
            $stmt = $this->conn->prepare('DELETE from petstore WHERE id = :petstore_id');
            $stmt->execute(array('petstore_id' => $petstore_id));
        } catch (PDOException $e) {
            echo "Error deleting PetStore " . $e->getMessage();
            return false;
        }
    }

}
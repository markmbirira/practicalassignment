<?php

require_once('../config/config.php');

class Database
{   
    private $db_dsn  = DB_DSN;
    private $db_user = DB_USER;
    private $db_pass = DB_PASSWORD;
    private $options = OPTIONS;

    protected $connection;

    public function openConnection()
    {
        try {
            $this->connection = new PDO($this->db_dsn, $this->$db_user, 
                                        $this->db_pass, $this->options);
            return $this->connection;

        } catch (PDOException $e) {
            echo "Error Establishing Connection " . $e->getMessage();
            return false;
        }
    }
 
    public function closeConnection()
    {
        $this->connection = NULL;
    }

}


<?php

require('../database/database.php');
require('../src/PetStore/petstore.php');
require('../src/PetStore/pet.php');
require('../src/PetStore/custodian.php');
require('../src/PetStore/actionAudit.php');

$dbObject = new Database();
$conn = $dbObject->openConnection();

$petStore = new PetStore($conn);
$pets     = new Pet($conn);
$custodin = new Custodian($conn);
$action   = new Actions($conn);

?>
<!doctype html>
<html>
    <head>
        <style>
            /* Style the header with a grey background and some padding */
            .header {
            overflow: hidden;
            background-color: #f1f1f1;
            padding: 20px 10px;
            }

            /* Style the header links */
            .header a {
            float: left;
            color: black;
            text-align: center;
            padding: 12px;
            text-decoration: none;
            font-size: 18px; 
            line-height: 25px;
            border-radius: 4px;
            }

            /* Style the logo link (notice that we set the same value of line-height and font-size to prevent the header to increase when the font gets bigger */
            .header a.logo {
            font-size: 25px;
            font-weight: bold;
            }

            /* Change the background color on mouse-over */
            .header a:hover {
            background-color: #ddd;
            color: black;
            }

            /* Style the active/current link*/
            .header a.active {
            background-color: dodgerblue;
            color: white;
            }

            /* Float the link section to the right */
            .header-right {
            float: right;
            }

            /* Add media queries for responsiveness - when the screen is 500px wide or less, stack the links on top of each other */ 
            @media screen and (max-width: 500px) {
            .header a {
                float: none;
                display: block;
                text-align: left;
            }
            .header-right {
                float: none;
            }
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="header">
                <a href="#default" class="logo">PetStore</a>
                <div class="header-right">
                    <a class="active" href="/views/petstore/petstores.php">PetStores</a>
                    <a href="/views/pet/pet.php">Pet Types</a>
                    <a href="/views/custodian/custodian.php">Custodians</a>
                </div>
            </div>
        </div>
<?php // rest of the page continues ?>

<div class="main">
    <div class="all-petstores-view">
        <p>All PetStores available</p>
    </div>
    <div class="all-pets-view">
        <p>All Pet types available</p>
    </div>
    <div class="all-custodians-view">
        <p>All Pet types available</p>
    </div>
</div>
